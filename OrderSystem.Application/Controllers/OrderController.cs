﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderSystem.Domain.Services;
using OrderSystem.Domain.ViewModels;

namespace OrderSystem.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;
        private readonly ILogger<OrderController> _logger;
        public OrderController(IOrderService orderService, ILogger<OrderController> logger) => (this.orderService, _logger) = (orderService, logger);

        [HttpGet]
        public async Task<IActionResult> GetOrderById(Guid orderId)
        {
            try
            {
                GetOrderView result = await orderService.GetOrderById(orderId);

                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(GetOrderById));
                return new ObjectResult("An Error occured while performing this request")
                {
                    StatusCode = 500
                };
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateOrder(CreateOrderView createOrder)
        {
            try
            {
                OrderCreatedView result = await orderService.CreateOrder(createOrder);
                return Ok(result);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(CreateOrder));
                return new ObjectResult("An Error occured while performing this request")
                {
                    StatusCode = 500
                };
            }
        }
    }
}
