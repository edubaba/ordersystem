﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderSystem.Domain.Dto
{
    public class OrderDto
    {
        [Key]
        public Guid OrderId { get; set; }
        public ICollection<OrderItemDto> OrderItems { get; set; }
    }
}
