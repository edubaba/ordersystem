﻿using OrderSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OrderSystem.Domain.Dto
{
    public class OrderItemDto
    {
        [Key]
        public Guid OrderItemId { get; set; }
        public ProductType ProductType { get; set; }
        public int Quantity { get; set; }
    }
}
