﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OrderSystem.Domain.Entities
{
    public class Order
    {
        public Order(List<Product> orderItems)
        {
            OrderItems = orderItems;
        }
        public List<Product> OrderItems { get; }
        public Width BinWidth { get => CalculateBinWidth(); }

        private Width CalculateBinWidth()
        {
            return new Width(OrderItems.Sum(o => o.Width.InMillimetres));
        }
    }
}
