﻿using OrderSystem.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.Entities
{
    public class Product
    {
        public Product(Quantity quantity)
        {
            Quantity = quantity;
        }

        public Quantity Quantity { get; }
        public ProductType ProductType { get; protected set; }
        public Width Width { get => CalculateProductWidth(); }

        protected virtual Width CalculateProductWidth()
        {
            return new Width(0);
        }
    }

    public class Calendar : Product
    {
        public Calendar(Quantity quantity) : base(quantity)
        {
            ProductType = ProductType.Calendar;
        }

        protected override Width CalculateProductWidth()
        {
            return new Width(10 * Quantity.Value);
        }
    }

    public class PhotoBook : Product
    {
        public PhotoBook(Quantity quantity): base(quantity)
        {
            ProductType = ProductType.PhotoBook;
        }

        protected override Width CalculateProductWidth()
        {
            return new Width(19 * Quantity.Value);
        }
    }

    public class Canvas : Product
    {
        public Canvas(Quantity quantiy) : base(quantiy)
        {
            ProductType = ProductType.Canvas;
        }

        protected override Width CalculateProductWidth()
        {
            return new Width(16 * Quantity.Value);
        }
    }

    public class Cards : Product
    {
        public Cards(Quantity quantity): base(quantity)
        {
            ProductType = ProductType.Cards;
        }

        protected override Width CalculateProductWidth()
        {
            return new Width(4.7 * Quantity.Value);
        }
    }

    public class Mug : Product
    {
        public Mug(Quantity quantity) : base(quantity)
        {
            ProductType = ProductType.Mug;
        }

        protected override Width CalculateProductWidth()
        {
            int multiple = Quantity.Value % 4 == 0 ? Quantity.Value / 4 : Quantity.Value / 4 + 1;
            return new Width(94 * multiple);
        }
    }
}
