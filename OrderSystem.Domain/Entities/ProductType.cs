﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.Entities
{
    public enum ProductType
    {
        PhotoBook, Calendar, Mug, Canvas, Cards
    }
}
