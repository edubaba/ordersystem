﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.Entities
{
    public class Quantity
    {
        public Quantity(int value) => Value = value;
        public int Value { get; set; }
    }
}
