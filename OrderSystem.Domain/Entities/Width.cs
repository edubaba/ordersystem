﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.Entities
{
    public class Width
    {
        public Width(double width)
        {
            InMillimetres = width;
        }
        public double InMillimetres { get; private set; }
    }
}
