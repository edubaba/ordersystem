﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.Exceptions
{
    [Serializable]
    public class ProductNotSupportedException : Exception
    {
        public ProductNotSupportedException() : base("Product not supported")
        {
        }
    }
}
