﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Domain.Repositories
{
    public interface IGenericRepository<T> where T : class
    {
        Task<T> GetAsync(Guid id);

        IQueryable<T> Query();

        Task InsertAsync(T entity);
    }
}
