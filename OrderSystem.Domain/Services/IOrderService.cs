﻿using OrderSystem.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Domain.Services
{
    public interface IOrderService
    {
        Task<OrderCreatedView> CreateOrder(CreateOrderView createOrderView);
        Task<GetOrderView> GetOrderById(Guid id);
    }
}
