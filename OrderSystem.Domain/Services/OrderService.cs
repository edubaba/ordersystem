﻿using OrderSystem.Domain.Dto;
using OrderSystem.Domain.Entities;
using OrderSystem.Domain.Repositories;
using OrderSystem.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Domain.Services
{
    public class OrderService : IOrderService
    {
        private readonly IGenericRepository<OrderDto> _repository;
        public OrderService(IGenericRepository<OrderDto> repository) => _repository = repository;
        public async Task<OrderCreatedView> CreateOrder(CreateOrderView createOrderView)
        {
            createOrderView.OrderId = Guid.NewGuid();
            var orderItems = createOrderView.OrderedItems.Select(o => new OrderItemDto
            {
                OrderItemId = Guid.NewGuid(),
                ProductType = o.ProductType,
                Quantity = o.Quantity
            }).ToList();
            var orderDto = new OrderDto
            {
                OrderId = createOrderView.OrderId,
                OrderItems = orderItems
            };
            await _repository.InsertAsync(orderDto);

            var order = MapOrderDtoToOrder(orderDto);

            var view = new OrderCreatedView
            {
                MinimumBinWidth = order.BinWidth.InMillimetres,
                OrderId = orderDto.OrderId
            };

            return view;
        }
        
        public async Task<GetOrderView> GetOrderById(Guid id)
        {
            var orderDto = await _repository.GetAsync(id);
            var order = MapOrderDtoToOrder(orderDto);

            var view = new GetOrderView
            {
                MinimumBinWidth = order.BinWidth.InMillimetres,
                OrderId = orderDto.OrderId,
                OrderedItems = order.OrderItems.Select(o => new OrderView
                {
                    ProductType = o.ProductType.ToString(),
                    Quantity = o.Quantity.Value
                }).ToList()
            };

            return view;
        }

        private Order MapOrderDtoToOrder(OrderDto dto)
        {
            List<Product> orderItems = dto.OrderItems.Select(o =>
            {
                {
                    switch (o.ProductType)
                    {
                        case ProductType.Calendar:
                            return new Calendar(new Quantity(o.Quantity));
                        case ProductType.PhotoBook:
                            return new PhotoBook(new Quantity(o.Quantity));
                        case ProductType.Canvas:
                            return new Canvas(new Quantity(o.Quantity));
                        case ProductType.Cards:
                            return new Cards(new Quantity(o.Quantity));
                        case ProductType.Mug:
                            return new Mug(new Quantity(o.Quantity));
                        default:
                            return new Product(new Quantity(o.Quantity));
                    }
                }
                
            }).ToList();
            return new Order(orderItems);
        }
    }
}
