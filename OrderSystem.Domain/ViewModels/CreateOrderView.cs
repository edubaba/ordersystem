﻿using OrderSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.ViewModels
{
    public class CreateOrderView
    {
        public Guid OrderId { get; set; }
        public List<ProductItemView> OrderedItems { get; set; }
    }

    public class ProductItemView
    {
        public ProductType ProductType { get; set; }
        public int Quantity { get; set; }
    }

    public class OrderCreatedView
    {
        public Guid OrderId { get; set; }
        public double MinimumBinWidth { get; set; }
    }
}
