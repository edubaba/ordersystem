﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Domain.ViewModels
{
    public class GetOrderView
    {
        public Guid OrderId { get; set; }
        public List<OrderView> OrderedItems { get; set; }
        public double MinimumBinWidth { get; set; }
    }

    public class OrderView
    {
        public string ProductType { get; set; }
        public int Quantity { get; set; }
    }
}
