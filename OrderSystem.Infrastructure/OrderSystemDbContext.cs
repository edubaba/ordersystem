﻿using Microsoft.EntityFrameworkCore;
using OrderSystem.Domain.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace OrderSystem.Infrastructure
{
    public class OrderSystemDbContext : DbContext
    {
        public OrderSystemDbContext(DbContextOptions<OrderSystemDbContext> options) : base(options)
        {
            this.ChangeTracker.LazyLoadingEnabled = false;
        }
        public DbSet<OrderDto> Orders { get; set; }
        public DbSet<OrderItemDto> OrderItems { get; set; }
    }
}
