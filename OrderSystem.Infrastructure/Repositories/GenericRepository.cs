﻿using OrderSystem.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Infrastructure.Repositories
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        public GenericRepository(OrderSystemDbContext orderSystemDbContext)
        {
            DbContext = orderSystemDbContext;
            DbContext.Database.EnsureCreated();
        }
        protected OrderSystemDbContext DbContext { get; set; }
        public virtual async Task<T> GetAsync(Guid id)
        {
            return await DbContext.FindAsync<T>(id);
        }

        public virtual async Task InsertAsync(T entity)
        {
            DbContext.Set<T>().Add(entity);
            await DbContext.SaveChangesAsync();
        }

        public IQueryable<T> Query()
        {
            return DbContext.Set<T>().AsQueryable();
        }
    }
}
