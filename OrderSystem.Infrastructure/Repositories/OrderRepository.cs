﻿using Microsoft.EntityFrameworkCore;
using OrderSystem.Domain.Dto;
using OrderSystem.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Infrastructure.Repositories
{
    public class OrderRepository : GenericRepository<OrderDto>, IGenericRepository<OrderDto>
    {
        public OrderRepository(OrderSystemDbContext orderSystemDbContext): base(orderSystemDbContext)
        {
        }

        public override async Task InsertAsync(OrderDto entity)
        {
                DbContext.Orders.Add(entity);
            await DbContext.SaveChangesAsync();
        }

        public override async Task<OrderDto> GetAsync(Guid id)
        {
            return await DbContext.Orders.Include(o => o.OrderItems).FirstOrDefaultAsync(od => od.OrderId == id);
        }
    }
}
