﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using OrderSystem.Application.Controllers;
using OrderSystem.Domain.Exceptions;
using OrderSystem.Domain.Services;
using OrderSystem.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrderSystem.Tests.ControllerTests
{
    public class OrderControllerTests
    {
        private readonly IOrderService orderService;
        private readonly Guid id;
        private OrderController orderController;
        public OrderControllerTests()
        {
            id = Guid.NewGuid();
            var orderServiceMock = new Mock<IOrderService>();
            orderServiceMock.Setup(o => o.GetOrderById(id)).Returns(Task.FromResult(new GetOrderView
            {
                MinimumBinWidth = 5d,
                OrderedItems = new List<OrderView>(),
                OrderId = id
            }));
            orderServiceMock.Setup(o => o.CreateOrder(new CreateOrderView { OrderedItems = new List<ProductItemView>() })).Returns(Task.FromResult(new OrderCreatedView()));
            orderService = orderServiceMock.Object;
            orderController = new OrderController(orderService);
        }

        [Fact]
        public async Task GetOrderById_Returns_200()
        {
            var result = await orderController.GetOrderById(id) as ObjectResult;

            Assert.Equal(200, result.StatusCode);
        }

        [Fact]
        public async Task CreateOrder_Returns_200()
        {
            var result = await orderController.CreateOrder(new CreateOrderView
            {
                OrderedItems = new List<ProductItemView>()
            }) as ObjectResult;

            Assert.Equal(200, result.StatusCode);
        }
    }
}
