﻿using Moq;
using OrderSystem.Domain.Dto;
using OrderSystem.Domain.Entities;
using OrderSystem.Domain.Repositories;
using OrderSystem.Domain.Services;
using OrderSystem.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrderSystem.Tests.DomainTests
{
    public class OrderServiceTests
    {
        [Fact]
        public async Task GetOrder_Returns_Order()
        {
            Guid id = Guid.NewGuid();
            var orderRepositoryMock = new Mock<IGenericRepository<OrderDto>>();
            orderRepositoryMock.Setup(o => o.GetAsync(id)).Returns(Task.FromResult(new OrderDto
            {
                OrderId = id,
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto()
                    {
                        ProductType = ProductType.Calendar,
                        OrderItemId = Guid.NewGuid(),
                        Quantity = 5
                    },
                    new OrderItemDto()
                    {
                        ProductType = ProductType.Cards,
                        OrderItemId = Guid.NewGuid(),
                        Quantity = 3
                    }
                }
            }));
            OrderService orderService = new OrderService(orderRepositoryMock.Object);

            GetOrderView result = await orderService.GetOrderById(id);

            Assert.True(result.OrderedItems.Count == 2);
            Assert.True(result.MinimumBinWidth > 0);
        }

        [Fact]
        public async Task CreateOrder_CreatesOrder()
        {
            Guid id = Guid.NewGuid();
            var orderDto = new OrderDto
            {
                OrderId = id,
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto()
                    {
                        ProductType = ProductType.Calendar,
                        OrderItemId = Guid.NewGuid(),
                        Quantity = 5
                    },
                    new OrderItemDto()
                    {
                        ProductType = ProductType.Cards,
                        OrderItemId = Guid.NewGuid(),
                        Quantity = 3
                    }
                }
            };
            var orderRepositoryMock = new Mock<IGenericRepository<OrderDto>>();
            orderRepositoryMock.Setup(o => o.InsertAsync(orderDto)).Returns(Task.CompletedTask);
            var orderService = new OrderService(orderRepositoryMock.Object);

            var result = await orderService.CreateOrder(new CreateOrderView
            {
                OrderedItems = new List<ProductItemView>
                {
                    new ProductItemView
                    {
                        ProductType = ProductType.Calendar,
                        Quantity = 5
                    },
                    new ProductItemView
                    {
                        ProductType = ProductType.Cards,
                        Quantity = 3
                    }
                }
            });

            Assert.Equal(64.1, result.MinimumBinWidth);
        }
    }
}
