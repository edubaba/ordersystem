﻿using OrderSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace OrderSystem.Tests.DomainTests
{
    public class OrderTests
    {
        [Fact]
        public void Order_BinWidth_Returns_Sum_Of_Items_Width()
        {
            var orderItems = new List<Product>
            {
                new Mug(new Quantity(3)),
                new PhotoBook(new Quantity(4)),
                new Canvas(new Quantity(1))
            };
            var order = new Order(orderItems);

            Assert.Equal(186, order.BinWidth.InMillimetres);
        }
    }
}
