﻿using OrderSystem.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace OrderSystem.Tests.DomainTests
{
    public class ProductTests
    {
        [Theory]
        [InlineData(3, ProductType.Calendar, 30)]
        [InlineData(4, ProductType.Mug, 94)]
        public void OrderItem_Width_Is_a_Multiple_Of_Quantity(int quantity, ProductType productType, int result)
        {
            // Arrange
            Product product = null;
            if (productType == ProductType.Calendar)
                product = new Calendar(new Quantity(quantity));
            else if (productType == ProductType.Mug)
                product = new Mug(new Quantity(quantity));
            else
                product = new Product(new Quantity(quantity));

            // Assert
            Assert.Equal(result, product.Width.InMillimetres);
        }
    }
}
