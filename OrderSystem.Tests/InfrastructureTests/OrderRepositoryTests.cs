﻿using Microsoft.EntityFrameworkCore;
using OrderSystem.Domain.Dto;
using OrderSystem.Domain.Entities;
using OrderSystem.Infrastructure;
using OrderSystem.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace OrderSystem.Tests.InfrastructureTests
{
    public class OrderRepositoryTests
    {
        private readonly DbContextOptions<OrderSystemDbContext> options;
        private readonly OrderSystemDbContext _context;
        private readonly OrderRepository orderRepository;
        private Guid id;

        public OrderRepositoryTests()
        {
            id = Guid.NewGuid();
            var builder = new DbContextOptionsBuilder<OrderSystemDbContext>();
            builder.UseInMemoryDatabase("OrderSystem");
            options = builder.Options;
            _context = new OrderSystemDbContext(options);
            orderRepository = new OrderRepository(_context);
            SeedDatabase();
        }

        [Fact]
        public async Task InsertAsync_Inserts_Entity()
        {
            var orderId = Guid.NewGuid();
            await orderRepository.InsertAsync(new OrderDto
            {
                OrderId = orderId,
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto
                    {
                        OrderItemId = Guid.NewGuid(),
                        ProductType = ProductType.Calendar,
                        Quantity = 3
                    }
                }
            });

            var order = await orderRepository.GetAsync(orderId);

            Assert.NotNull(order);
            Assert.NotNull(order.OrderItems);
        }

        [Fact]
        public async Task Get_Returns_Entity()
        {
            OrderDto order = await orderRepository.GetAsync(id);
            Assert.NotNull(order);
            Assert.Equal(id, order.OrderId);
            Assert.True(order.OrderItems.Count > 0);
        }

        private void SeedDatabase()
        {
            _context.Orders.Add(new OrderDto
            {
                OrderId = id,
                OrderItems = new List<OrderItemDto>
                {
                    new OrderItemDto
                    {
                        OrderItemId = Guid.NewGuid(),
                        ProductType = ProductType.Canvas,
                        Quantity = 2
                    },
                    new OrderItemDto
                    {
                        OrderItemId = Guid.NewGuid(),
                        ProductType = ProductType.Mug,
                        Quantity = 20
                    }
                }
            });
            _context.SaveChanges();
        }
    }
}
